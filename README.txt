Introduction
=============
Block Page Visibility enables site developers to centralize the display of blocks to a single PHP function. It is an alternative to controlling display via each block's configure form. The more "sometimes on, sometimes off blocks" that a site uses, the more useful this module becomes.

Usage
=============
Before activitating this module, you want to edit the bpv_config() function which comes at he bottom of bpv.module. You may uncomment it there or copy the function to a module of your own.

Credits
===========
Barry Jaspan (http://jaspan.com)
Moshe Weitzman (http://drupal.org/moshe)

Initial sponsorship by New York Observer (see http://drupal.org/nyobserver)
Further sponsorship from Flisolo